# This project has been moved to GitHub!

You can now find it here: [https://github.com/alwaysblank/cyrus](https://github.com/alwaysblank/cyrus)

This repo will remain up for the foreseeable future, but will no longer receive updates.